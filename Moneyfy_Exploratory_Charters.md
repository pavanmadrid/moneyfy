<h3>Moneyfy app Exploratory tests charters </h3>

Format :- Explore target, with resources, to discover information


### Explore home screen
	
	### with home screen widgets
	
		### To learn what each widget does on the home screen
		
		### Time Box : 15 minutes
		
		### Test Notes:
		
		* Home screen has 12 category widgets to add expenses
		* Home screen also has 2 widgets at the top to either add an expense/income
		* I can see the 'updated balance' widget just below the 12 category widgets
		* At the centre there is also a graph representiation of total expenses/income 
		* At the top header section , I can select my account and set the calendar (day/week/month/year/all/customer interval date)
		* There's a search bar to search for expense/income records 
		* I can also  see a transfer icon which takes me to the transfer screen to record transsfers from my cash/card -> cash/card
		* And I also see the settings icon to modify the settings/configuration/properties of the app features
		

### Explore Settings screen
	
	### with catergories/accounts/currencies/settings widget 
	
		### To understand the app settings/configuration/properties
			
			
		### Time Box : 30 minutes
		
		### Test Notes:
		
		* The settings tab has 4 widgets to modify categories/accounts/currencies/settings
		* When I click Categories , I get an option to add a new expense category if I am on pro-version
		* Also on categories, I can modify the existing expenses for example the name and those changes would be reflected across the app
		* I can do the same for incomes as well - add a new income category if am on pro-version or I can choose to edit existing income categories.
		* Going back to the settings tab , I now click on the accounts widget.
		* Here I can add a new account category if am on pro-version.
		* I can also choose to edit the current account categories like the account currency/balance/date.
		* Clicking on currency widget of settings screen takes me to a list of pro-features related to currency.
		* I then click on settings widget and I see options to change balance/general settings. 
		* I explore more on general settings menu. I first click on the theme to select the darker option which is available only on pro-version.
		* I can however change the language and I have the option to choose 8 different international languages.
		* I select the language Russian for example and I do see that the app language changes to Russian.
		* Next I click on the currency and I see a widget with a search option for desired currency. I key in USD and I am able to select and switch my app currency to USD.
		* I am also able to change the 'first day of week' option and see that the change is reflected across the app.
		

### Explore Expenses
	### with expense category widgets
		###	To understand how expenses work
			
	
		### Time box : 15 minutes
		
		### Test Notes:
		
		* I am able to add an expense for the 'food' category
		* I see that the maximum digts accepted for the expense is 9.
		* Maximum decimal places accepted for expense is 2.
		* I similary add expenses for category Sports,House,Pets,Transport,Entertainment,Clothes,Taxi,Toiletry,Car,Eating Out,Health.
		* I am able to see the correct updated total expenses at the centre.
		

### Explore Incomes
	### With income '+' widget
		### To understand incomes functionality
		 
		### Time box : 15 minutes
		 
		### Test Notes:
		 
		 * When I click on the + icon , I am taken to incomes screen where I can key in an income and then choose a category.
		 * Here again I notice that the maximum digits allowed for the income is 9 and the decimal places allowed is 2.
		 * When I click on choose category , I can see 'Deposits','Salary' and 'Savings' categories .
		 * I also have the the option of editing my expnse amount again and an option to add notes which are optional.
		 * Also there's a + icon to add a new income cateogy if you are on pro-version.



### Explore Transfers
	### With income 'transfer' widget
		### To understand transfers functionality
		 
		### Time box : 15 minutes
		 
		### Test Notes:
		 
		 * When I click on the 'transfers' icon , I am taken to transfers screen where I can key in an amount and initiate transfer between card/cash.
		 * Here again I notice that the maximum digits allowed for the income is 9 and the decimal places allowed is 2.
		 * I can choose the source and target for the transfer. The options being Payment Card/ Cash.
		 

### Explore DataBackup
	### With income 'settings' -> 'Data backup' widget
		### To understand data-backup functionality
		 
		### Time box : 15 minutes
		 
		### Test Notes:
		 
		 * When I click on the 'settings' icon , I am taken to settings screen where I see options for data back up at the bottom.
		 * I click on 'Create Data backup' and I see that I have an option to save my data to Google Drive.
		 * I am able to succesfully export my data to Google drive as back-up.
		 * There's also an option for me restore the saved data.I click on it and am able to restore my previously saved back-up.
		 * I also see an option to clear/delete my existing data. Clicking on it , I notice that all my data is cleared.
